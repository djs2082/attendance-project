from django.contrib import admin
from django.apps import apps
from students import models

admin.site.register(models.Subject)
admin.site.register(models.Year)
admin.site.register(models.Semester)
admin.site.register(models.Section)
admin.site.register(models.Class)
admin.site.register(models.Professor)
admin.site.register(models.Department)
admin.site.register(models.Student)
admin.site.register(models.StudentAttendance)


